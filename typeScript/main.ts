type Person = {
	name: string;
	age: number;
  };
   
  function greet(person: Person) {
	return "Hello " + person.name + " You are " + person.age + " years old";
}

let johnDoe = {
	name: 'john',
	age: 23
}
console.log(greet(johnDoe))