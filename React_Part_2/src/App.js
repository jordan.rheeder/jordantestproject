import React, { useMemo, useState, useRef, useEffect } from "react";
import { render } from "react-dom";
import "./styles.css";

/*
========== TODO ==========
1. Use the input to type in a name - done
2. implement useMemo on slowNumber and the "slow" function.
3. Conditional rendering
4. Update the renderCount state to count the number of re-renders. - Done
*/

//Pretend this is a reeeealy slow function
function slowFunction(number) {
  return number * 2;
}
export default function App() {
  //Fix us please
  // let inputText = "";
  const [inputText, setInputText] = useState("");
  const renderCount = useRef(0);
  const [slowNumber, setSlowNumber] = useState(1);

  const temp = useMemo(() => {
    return slowFunction(slowNumber);
  }, [slowNumber]);

  const [firstHidden, setFirstHidden] = useState(false);

  // Placeholder for the slowFunction call.

  //Hmmmmm how do we update the renderCount, is there a hook we can use??
  useEffect(() => {
    renderCount.current++;
    console.log(renderCount);
  });
  return (
    <div className="App">
      <h1>Session 2: Hooks</h1>
      <input value={inputText} onChange={(e) => setInputText(e.target.value)} />
      {/*is the onChange correct?*/}
      <div className="marginDiv">I rendered {renderCount.current} times</div>
      <hr />
      <div className="marginDiv">Slow function: {slowNumber}</div>
      <button onClick={() => setSlowNumber(temp)}>Click me</button>
      <hr />
      {/*Hide the*/}
      <div className="marginDiv">
        {
          <>
            {firstHidden ? (
              <div>Hide me if below div is shown</div>
            ) : (
              <div>Show me is above div is hidden</div>
            )}
            <button onClick={(e) => setFirstHidden(!firstHidden)}>
              {firstHidden.toString().substr(0, 1).toLocaleUpperCase(0) +
                firstHidden
                  .toString()
                  .substr(1, firstHidden.toString().length - 1)}
            </button>
          </>
        }
      </div>
    </div>
  );
}
